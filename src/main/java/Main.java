import gui.Info;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import parse.Group;
import parse.Parser;
import save.ExcelTable;
import save.PathAdapter;
import save.Table;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Info.WINDOW.println("Загрузка драйвера...");
        System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");

        Info.WINDOW.println("Настройка драйвера...");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        WebDriver driver = new ChromeDriver(options);

        Info.WINDOW.println("Загрузка сайта...");
        driver.get("https://www.myscore.ru/");

        Info.WINDOW.println("Сбор информации с сайта результатов, это может занять несколько минут...");
        List<WebElement> webElements = Parser.getHtmlElements(driver);
        List<Group> groups = Parser.createGroups(webElements);

        Info.WINDOW.println("Обработка результатов...");
        String filePath = PathAdapter.getFilePath(args[0], args[1]);
        Table table = new ExcelTable(filePath);

        Info.WINDOW.println("Сохранение результатов...");
        table.save(groups);

        Info.WINDOW.println("Закрытие драйвера...");
        driver.quit();

        Info.WINDOW.println("Готово!");
    }
}

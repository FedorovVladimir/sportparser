package gui;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Класс одиночка для вывода информации
 * @author @author FedorovVladimir https://gitlab.com/FedorovVladimir
 */
public enum Info {
    WINDOW;

    public void println(String text) {
        print(text + "\n");
    }

    public void print(String text) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH.mm.ss");
        System.out.print(dateFormat.format(new Date()) + " " + text);
    }
}

package parse;

public class Event {
    public String time;
    public String date;
    public String participantHome;
    public String participantAway;
    public String score;
}

package parse;

import java.util.LinkedList;
import java.util.List;

public class Group {
    public String name;
    public List<Event> matches = new LinkedList<Event>();

    Group() {

    }

    public Group (String name) {
        this.name = name;
    }
}



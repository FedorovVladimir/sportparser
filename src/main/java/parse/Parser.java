package parse;

import gui.Info;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author @user-penguin https://gitlab.com/
 */
public class Parser {

    private static int number;

    // создание заполненных групп
    public static List<Group> createGroups (List<WebElement> source) {
        String currentDate = getDate();
        List<Group> resultGroups = new LinkedList<Group>();
        Group group = new Group();
        for (WebElement element: source) {
            if (isHeader(element)) {
                if (group.matches.size() > 0) {
                    resultGroups.add(group);
                }
                group = new Group(getGroupName(element));
                continue;
            }
            Event match = createMatch(element);
            match.date = currentDate;
            group.matches.add(match);
        }
        return resultGroups;
    }

    private static Event createMatch (WebElement element) {
        Event match = new Event();
        match.score = element.findElement(By.cssSelector("div.event__scores")).getText();
        match.participantHome = element.findElement(By.cssSelector("div.event__participant.event__participant--home")).getText();
        match.participantAway = element.findElement(By.cssSelector("div.event__participant.event__participant--away")).getText();
        match.time = findTime(element.getText());

        if (match.score.equals("")) {
            Info.WINDOW.println("ERROR score");
        }
        if (match.participantHome.equals("")) {
            Info.WINDOW.println("ERROR participantHome");
        }
        if (match.participantAway.equals("")) {
            Info.WINDOW.println("ERROR participantAway");
        }
        if (match.time.equals("")) {
            Info.WINDOW.println("ERROR time");
        }

        if (++number % 10 == 0) {
            Info.WINDOW.println("Считано " + number + " матчей");
        }

        return match;
    }

    private static boolean isHeader (WebElement element) {
        String regexpHeader = "(^event__header$)|(^event__header.+)";
        Pattern pattern = Pattern.compile(regexpHeader);
        Matcher matcher = pattern.matcher(element.getAttribute("class"));
        return matcher.matches();
    }

    private static String getGroupName (WebElement element) {
        String groupName = "";
        // страна
        groupName += element.findElement(By.cssSelector("div.icon--flag")).getAttribute("title") + ": ";
        groupName += element.findElement(By.cssSelector("span.event__title--name")).getText();

        return groupName;
    }

    private static String getDate() {
        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy");
        return formatForDateNow.format(dateNow);
    }

    static String findTime(String source) {
        String regexpClock = "(^[0-9][0-9]:[0-9][0-9])";
        String regexpTimer = "(^[0-9]?[0-9]?[0-9]\\+?[0-9]?'?)";
        String regexpAnother = "(^[Пп]ерерыв)|(^Завершен)|(^Перенесен)|(^Отменен)";
        String regexpAfterSP = "(^После\nс\\.п\\.)";
        String regexpAfterOg = "(^Ожидание\nобновления)";
        String regexpPenaltiSerial = "(^Серия\nпенальти)";
        Pattern pattern = Pattern.compile(regexpClock);
        Matcher matcher = pattern.matcher(source);
        if (matcher.find()) {
            return matcher.group();
        }

        pattern = Pattern.compile(regexpTimer);
        matcher = pattern.matcher(source);
        if (matcher.find()) {
            return matcher.group() + "'";
        }

        pattern = Pattern.compile(regexpAnother);
        matcher = pattern.matcher(source);
        if (matcher.find()) {
            return matcher.group();
        }

        pattern = Pattern.compile(regexpAfterSP);
        matcher = pattern.matcher(source);
        if (matcher.find())
            return "После с.п.";

        pattern = Pattern.compile(regexpAfterOg);
        matcher = pattern.matcher(source);
        if (matcher.find())
            return "Ожидание обновления";

        pattern = Pattern.compile(regexpPenaltiSerial);
        matcher = pattern.matcher(source);
        if (matcher.find())
            return "Серия пенальти";

        return "";
    }

    public static List<WebElement> getHtmlElements (WebDriver driver) {

        // открытие настроек сайта
        WebElement settingsButton = driver.findElement(By.cssSelector(".header__button.header__button--settings"));
        settingsButton.click();

        // раскрытие групп
        WebElement yesRadioButton = driver.findElement(By.xpath("//input[contains(@name, 'mggroups') and contains(@value, 'true')]"));
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        yesRadioButton.click();

        // закрытие настроек
        WebElement closeButton =  driver.findElement(By.xpath(".//a[@id='lsid-window-close']"));
        closeButton.click();

        // получение матчей и их хедеров
        return driver.findElements(By.cssSelector("div.event__match, div.event__header"));
    }
}

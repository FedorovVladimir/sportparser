package save;

import parse.Group;

import java.util.List;

/**
 * Абстрактный класс для вывода результатов
 * @author FedorovVladimir https://gitlab.com/FedorovVladimir
 */
public abstract class AllTable implements Table {

    /**
     * Заголовки столбцов таблицы
     */
    String[] strings = new String[]{"СРВ", "Дата", "Время", "КМНД1", "КМНД2", "СЧЕТ"};

    /**
     * Сохранение списка групп
     * @param groups список элементов типа Group
     * @see Group
     */
    public void save(List<Group> groups) {
        saveHeader();
        for (Group group: groups) {
            save(group);
        }
    }

    /**
     * Создаем подписи к столбцам
     */
    abstract void saveHeader();

    /**
     * Сохранение очередной группы событий начиная с текущей строки
     * @param group Сохраняемая группа
     * @see Group
     */
    abstract void save(Group group);
}

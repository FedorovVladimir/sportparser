package save;

import parse.Event;
import parse.Group;

/**
 * Класс для вывода результатов в консоль
 * @author FedorovVladimir https://gitlab.com/FedorovVladimir
 */
public class ConsoleTable extends AllTable {

    @Override
    void saveHeader() {
        save(strings);
    }

    @Override
    void save(Group group) {
        for (Event event: group.matches) {
            String[] strings = {group.name, event.date, event.time, event.participantAway, event.participantHome, event.score};
            save(strings);
        }
    }

    private void save(String[] strings) {
        System.out.printf("%-50s", strings[0]);
        System.out.printf("%15s", strings[1] + "|");
        System.out.printf("%10s", strings[2] + " | ");
        System.out.printf("%-30s", strings[3]);
        System.out.printf("%-30s", "|" + strings[4]);
        System.out.printf("%-15s", "|" + strings[5]);
        System.out.println();
    }
}

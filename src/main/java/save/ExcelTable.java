package save;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import parse.Event;
import parse.Group;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Класс для сохранения результатов в таблицу Excel
 * @author FedorovVladimir https://gitlab.com/FedorovVladimir
 */
public class ExcelTable extends AllTable {

    /**
     * Путь до директории для сохранения таблицы
     */
    private String filePath;

    /**
     * Таблица excel файла в памяти
     */
    private HSSFWorkbook workbook = new HSSFWorkbook();

    /**
     * Лист таблицы
     */
    private HSSFSheet sheet = workbook.createSheet("Матчи");

    /**
     * Текущая строка таблицы
     */
    private int rowNum;

    /**
     * Стиль ячейки 1
     */
    private HSSFCellStyle styleCell1 = workbook.createCellStyle();

    /**
     * Стиль ячейки 2
     */
    private HSSFCellStyle styleCell2 = workbook.createCellStyle();

    /**
     * Стиль заголовков столбцов
     */
    private HSSFCellStyle styleHead = workbook.createCellStyle();

    public ExcelTable(String filePath) {
        this.filePath = filePath;

        styleCell1.setBorderBottom(CellStyle.BORDER_THIN);
        styleCell1.setBorderLeft(CellStyle.BORDER_THIN);
        styleCell1.setBorderRight(CellStyle.BORDER_THIN);
        styleCell1.setBorderTop(CellStyle.BORDER_THIN);
        styleCell1.setWrapText(true);

        styleCell2.setBorderBottom(CellStyle.BORDER_THIN);
        styleCell2.setBorderLeft(CellStyle.BORDER_THIN);
        styleCell2.setBorderRight(CellStyle.BORDER_THIN);
        styleCell2.setBorderTop(CellStyle.BORDER_THIN);
        styleCell2.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        styleCell2.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styleCell2.setWrapText(true);

        HSSFFont font = workbook.createFont();
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        styleHead.setFont(font);
        styleHead.setBorderBottom(CellStyle.BORDER_THIN);
        styleHead.setBorderLeft(CellStyle.BORDER_THIN);
        styleHead.setBorderRight(CellStyle.BORDER_THIN);
        styleHead.setBorderTop(CellStyle.BORDER_THIN);
        styleHead.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        styleHead.setFillPattern(CellStyle.SOLID_FOREGROUND);
    }

    @Override
    public void save(List<Group> groups) {
        super.save(groups);
        autoSize();
        createFile();
    }

    /**
     * Ресайз всех колонок итоговой таблицы по ширине
     */
    private void autoSize() {
        for (int i = 0; i < strings.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    /**
     * Создание таблицы в зананой папке с данным именем
     */
    private void createFile() {
        try {
            FileOutputStream out = new FileOutputStream(new File(filePath));
            workbook.write(out);
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Файл недоступен.\nВозможно он открыт в другой программе или путь указан не венро.\nЗакройте и попробуйте снова.", "Не возможно записать файл", JOptionPane.INFORMATION_MESSAGE);
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    void saveHeader() {
        Row row = sheet.createRow(rowNum);
        for (int i = 0; i < strings.length; i++) {
            row.createCell(i).setCellValue(strings[i]);
            row.getCell(i).setCellStyle(styleHead);
        }
    }

    @Override
    void save(Group group) {
        HSSFCellStyle styleCell;
        for (Event event: group.matches) {
            rowNum++;
            Row row = sheet.createRow(rowNum);
            String[] strings = {group.name, event.date, event.time, event.participantAway, event.participantHome, event.score};

            if (rowNum % 2 == 0) {
                styleCell = styleCell1;
            } else {
                styleCell = styleCell2;
            }
            for (int i = 0; i < strings.length; i++) {
                row.createCell(i).setCellValue(strings[i]);
                row.getCell(i).setCellStyle(styleCell);
            }
        }
    }
}

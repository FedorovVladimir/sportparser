package save;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Класс-адаптер добавляет к имени файла дату, время, расширение файла и директорию расположения
 * @author @author FedorovVladimir https://gitlab.com/FedorovVladimir
 */
public class PathAdapter {

    /**
     * @param name имя файла
     * @return имя файла + дата + время в формате dd.MM.yyyy_HH.mm
     */
    static String rename(String name) {
        if (name.equals("")) {
            name = "testTable";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy_HH.mm");
        name += "_" + dateFormat.format(new Date());
        return name;
    }

    /**
     * @param pathDirectory путь к директории сохранения
     * @param nameFile имя файла
     * @return директория + имя файла + .xls
     */
    public static String getFilePath(String pathDirectory, String nameFile) {
        nameFile = rename(nameFile);
        if (!pathDirectory.equals("")) {
            nameFile = pathDirectory + "/" + nameFile;
        }
        return nameFile + ".xls";
    }
}

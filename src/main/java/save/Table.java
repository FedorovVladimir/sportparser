package save;

import parse.Group;

import java.util.List;

/**
 * Интерфейс для сохранения данных
 * @author FedorovVladimir https://gitlab.com/FedorovVladimir
 */
public interface Table {

    /**
     * @param groups Непустой список групп событий
     * @see Group
     */
    void save(List<Group> groups);
}

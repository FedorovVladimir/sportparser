package parse;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParserTest {

    private static WebDriver driver;

    static {
        System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        driver.get("https://www.myscore.ru/");
    }

    @Test
    void regExpClockTest() {
        String testString1 = "11:00Hijifigieg";
        assertEquals("11:00", Parser.findTime(testString1));
        String testString2 = "72Hijifigieg";
        assertEquals("72'", Parser.findTime(testString2));
        String testString3 = "перерывHijifigieg";
        assertEquals("перерыв", Parser.findTime(testString3));
        String testString4 = "ПерерывHijifigieg";
        assertEquals("Перерыв", Parser.findTime(testString4));
        String testString5 = "45+1Hijifigieg";
        assertEquals("45+1'", Parser.findTime(testString5));
        String testString6 = "ПеренесенHijifigieg";
        assertEquals("Перенесен", Parser.findTime(testString6));
        String testString7 = "После\nс.п.Hijifigieg";
        assertEquals("После с.п.", Parser.findTime(testString7));
        String testString8 = "Ожидание\nобновленияijifigieg";
        assertEquals("Ожидание обновления", Parser.findTime(testString8));
        String testString9 = "Серия\nпенальтиHijifigieg";
        assertEquals("Серия пенальти", Parser.findTime(testString9));
    }

    @Disabled
    @Test
    void testCreatingGroups () {
        Parser.createGroups(Parser.getHtmlElements(driver));
    }
}
package save;

import org.junit.jupiter.api.Test;
import parse.Event;
import parse.Group;

import java.util.Arrays;

class ExcelTableTest {

    @Test
    void saveOneGroup() {
        Group group = new Group("группа");
        Event event = new Event();
        event.time = "10:00";
        event.date = "02.11.00";
        event.score = "1-0";
        event.participantHome = "f1";
        event.participantAway = "f2";
        group.matches = Arrays.asList(event, event);
        Table table = new ExcelTable(PathAdapter.getFilePath("src/test/resources", "Матчи"));
        table.save(Arrays.asList(group, group));
    }
}
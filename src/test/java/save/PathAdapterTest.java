package save;

import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PathAdapterTest {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy_HH.mm");

    @Test
    void rename() {
        assertEquals(PathAdapter.rename("testTable"), "testTable" + "_" + dateFormat.format(new Date()));
        assertEquals(PathAdapter.rename(""), "testTable" + "_" + dateFormat.format(new Date()));
        assertEquals(PathAdapter.rename("table"), "table" + "_" + dateFormat.format(new Date()));
        assertEquals(PathAdapter.rename("Матчи"), "Матчи" + "_" + dateFormat.format(new Date()));
    }

    @Test
    void getFilePath() {
        assertEquals(PathAdapter.getFilePath("home", "Матчи"), "home/Матчи" + "_" + dateFormat.format(new Date()) + ".xls");
        assertEquals(PathAdapter.getFilePath("", "Матчи"), "Матчи" + "_" + dateFormat.format(new Date()) + ".xls");
        assertEquals(PathAdapter.getFilePath("", ""), "testTable" + "_" + dateFormat.format(new Date()) + ".xls");
    }
}